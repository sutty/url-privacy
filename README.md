# url-privacy

Removes tracking parameters from URLs.

## Installation

Add this line to your Gemfile:

```ruby
gem 'url-privacy'
```

And then execute:

    $ bundle

Or install it as:

    $ gem install url-privacy

## Usage

```yaml
UrlPrivacy.clean url
```

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/url-privacy>. This project is intended to be
a safe, welcoming space for collaboration, and contributors are expected
to adhere to the [Sutty code of
conduct](https://sutty.nl/en/code-of-conduct/).

If you like our plugins, [please consider
donating](https://donaciones.sutty.nl/en/)!

## License

The gem is available as free software under the terms of the LGPL3
License.

## Code of Conduct

Everyone interacting in the url-privacy project’s codebases, issue
trackers, chat rooms and mailing lists is expected to follow the [code
of conduct](https://sutty.nl/en/code-of-conduct/).
